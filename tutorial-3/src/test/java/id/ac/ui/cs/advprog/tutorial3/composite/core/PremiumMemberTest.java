package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        String name = member.getName();
        assertEquals(name, "Wati");
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        String role = member.getRole();
        assertEquals(role, "Gold Merchant");
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        int beforeAdd = member.getChildMembers().size();
        Member memberOrdinary = new OrdinaryMember("ordi", "o");
        Member memberPremium = new PremiumMember("pre", "p");
        member.addChildMember(memberOrdinary);
        member.addChildMember(memberPremium);
        int afterAdd = member.getChildMembers().size();
        assertEquals(beforeAdd + 2, afterAdd);
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member memberOrdinary = new OrdinaryMember("ordi", "o");
        Member memberPremium = new PremiumMember("pre", "p");
        member.addChildMember(memberOrdinary);
        member.addChildMember(memberPremium);
        int beforeDelete = member.getChildMembers().size();
        member.removeChildMember(memberOrdinary);
        member.removeChildMember(memberPremium);
        int afterDelete = member.getChildMembers().size();
        assertEquals(beforeDelete - 2, afterDelete);
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        assertEquals(0, member.getChildMembers().size());

        Member member1 = new OrdinaryMember("ordi1", "o");
        member.addChildMember(member1);
        assertEquals(1, member.getChildMembers().size());

        Member member2 = new OrdinaryMember("ordi2", "o");
        member.addChildMember(member2);
        assertEquals(2, member.getChildMembers().size());

        Member member3 = new OrdinaryMember("ordi3", "o");
        member.addChildMember(member3);
        assertEquals(3, member.getChildMembers().size());

        Member member4 = new OrdinaryMember("ordi4", "o");
        member.addChildMember(member4);
        assertEquals(3, member.getChildMembers().size());

        Member member5 = new OrdinaryMember("ordi5", "o");
        member.addChildMember(member5);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        assertEquals(0, member.getChildMembers().size());

        Member member1 = new OrdinaryMember("ordi1", "o");
        member.addChildGuildMaster(member1);
        assertEquals(1, member.getChildMembers().size());

        Member member2 = new OrdinaryMember("ordi2", "o");
        member.addChildGuildMaster(member2);
        assertEquals(2, member.getChildMembers().size());

        Member member3 = new OrdinaryMember("ordi3", "o");
        member.addChildGuildMaster(member3);
        assertEquals(3, member.getChildMembers().size());

        Member member4 = new OrdinaryMember("ordi4", "o");
        member.addChildGuildMaster(member4);
        assertEquals(4, member.getChildMembers().size());

        Member member5 = new OrdinaryMember("ordi5", "o");
        member.addChildGuildMaster(member5);
        assertEquals(5, member.getChildMembers().size());
    }
}
