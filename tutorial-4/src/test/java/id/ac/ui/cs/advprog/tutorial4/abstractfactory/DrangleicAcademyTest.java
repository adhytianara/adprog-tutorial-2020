package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNull;


public class DrangleicAcademyTest {
    KnightAcademy drangleicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        drangleicAcademy = new DrangleicAcademy();
        majesticKnight = drangleicAcademy.getKnight("majestic");
        metalClusterKnight = drangleicAcademy.getKnight("metal cluster");
        syntheticKnight = drangleicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightAcademyName() {
        // TODO create test
        String name = drangleicAcademy.getName();
        assertEquals(name, "Drangleic");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof Knight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Metal Armor", majesticKnight.getArmor().getDescription());
        assertEquals("Thousand Jacker", majesticKnight.getWeapon().getDescription());
        assertNull(majesticKnight.getSkill());

        assertEquals("Metal Armor", metalClusterKnight.getArmor().getDescription());
        assertEquals("Thousand Years Of Pain", metalClusterKnight.getSkill().getDescription());
        assertNull(metalClusterKnight.getWeapon());

        assertEquals("Thousand Years Of Pain", syntheticKnight.getSkill().getDescription());
        assertEquals("Thousand Jacker", syntheticKnight.getWeapon().getDescription());
        assertNull(syntheticKnight.getArmor());
    }

}
