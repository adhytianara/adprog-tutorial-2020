package id.ac.ui.cs.advprog.tutorial4.abstractfactory.service;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.repository.AcademyRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatObject;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AcademyServiceImplTest {

    @Mock
    private AcademyRepository academyRepository;

    @InjectMocks
    private AcademyServiceImpl academyService;

    // TODO create tests

    @Test
    public void whenGetKnightIsCalledBeforeProducingKnightItShouldReturnNull(){
        Knight knight = academyService.getKnight();
        assertThat(knight).isEqualTo(null);
    }

    @Test
    public void whenProduceKnightIsCalledItCreateNewKnightAndSetItToTheAttribute(){
        AcademyService academySpy = spy(academyService);
        when(academyRepository.getKnightAcademyByName("Lordran")).thenReturn(new LordranAcademy());
        academySpy.produceKnight("Lordran", "majestic");
        verify(academyRepository, times(1)).getKnightAcademyByName("Lordran");

        Knight knight = academyService.getKnight();

        when(academySpy.getKnight())
                .thenReturn(knight);

        Knight calledKnight = academySpy.getKnight();
        assertThat(calledKnight).isEqualTo(knight);
    }


    @Test
    public void whenGetKnightAcademiesIsCalledItShouldReturnListOfKnightAcademy() {
        List<KnightAcademy> knightAcademies = new ArrayList<>();
        knightAcademies.add(new LordranAcademy());
        knightAcademies.add(new DrangleicAcademy());

        AcademyService academySpy = spy(academyService);

        when(academySpy.getKnightAcademies())
                .thenReturn(knightAcademies);

        Iterable<KnightAcademy> calledKnightAcademy = academySpy.getKnightAcademies();
        assertThat(calledKnightAcademy).isEqualTo(knightAcademies);
    }

    @Test
    public void whenGetKnightAcademiesIsCalledItShouldCallAcademyRepositoryGetKnightAcademies(){
        academyService.getKnightAcademies();
        verify(academyRepository, atLeastOnce()).getKnightAcademies();
    }
}
