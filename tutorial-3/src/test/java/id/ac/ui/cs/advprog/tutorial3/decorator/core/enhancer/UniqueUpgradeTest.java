package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Shield;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class UniqueUpgradeTest {

    private UniqueUpgrade uniqueUpgrade;

    @BeforeEach
    public void setUp(){
        uniqueUpgrade = new UniqueUpgrade(new Shield());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = uniqueUpgrade.getName();
        assertEquals(name, "Shield");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String description = uniqueUpgrade.getDescription();
        assertEquals(description, "Heater Shield UniqueUpgrade");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = uniqueUpgrade.getWeaponValue();
        assertTrue(20 <= value && value <= 25);
    }
}
