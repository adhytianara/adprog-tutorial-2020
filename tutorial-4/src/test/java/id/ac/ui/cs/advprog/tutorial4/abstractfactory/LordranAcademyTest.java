package id.ac.ui.cs.advprog.tutorial4.abstractfactory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.DrangleicAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.LordranAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.KnightAcademy;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


public class LordranAcademyTest {
    KnightAcademy lordranicAcademy;
    Knight majesticKnight;
    Knight metalClusterKnight;
    Knight syntheticKnight;

    @BeforeEach
    public void setUp() {
        // TODO setup me
        lordranicAcademy = new LordranAcademy();
        majesticKnight = lordranicAcademy.getKnight("majestic");
        metalClusterKnight = lordranicAcademy.getKnight("metal cluster");
        syntheticKnight = lordranicAcademy.getKnight("synthetic");
    }

    @Test
    public void checkKnightAcademyName() {
        // TODO create test
        String name = lordranicAcademy.getName();
        assertEquals(name, "Lordran");
    }

    @Test
    public void checkKnightInstances() {
        // TODO create test
        assertTrue(majesticKnight instanceof Knight);
        assertTrue(metalClusterKnight instanceof Knight);
        assertTrue(syntheticKnight instanceof Knight);
    }

    @Test
    public void checkKnightNames() {
        // TODO create test
        assertEquals("Majestic Knight", majesticKnight.getName());
        assertEquals("Metal Cluster Knight", metalClusterKnight.getName());
        assertEquals("Synthetic Knight", syntheticKnight.getName());
    }

    @Test
    public void checkKnightDescriptions() {
        // TODO create test
        assertEquals("Shining Armor", majesticKnight.getArmor().getDescription());
        assertEquals("Shining Buster", majesticKnight.getWeapon().getDescription());
        assertNull(majesticKnight.getSkill());

        assertEquals("Shining Armor", metalClusterKnight.getArmor().getDescription());
        assertEquals("Shining Force", metalClusterKnight.getSkill().getDescription());
        assertNull(metalClusterKnight.getWeapon());

        assertEquals("Shining Force", syntheticKnight.getSkill().getDescription());
        assertEquals("Shining Buster", syntheticKnight.getWeapon().getDescription());
        assertNull(syntheticKnight.getArmor());
    }

}
