package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GuildTest {
    private Guild guild;
    private Member guildMaster;

    @BeforeEach
    public void setUp() {
        guildMaster = new PremiumMember("Eko", "Master");
        guild = new Guild(guildMaster);
    }

    @Test
    public void testMethodAddMember() {
        //TODO: Complete me
        int beforeAdd = guild.getMemberList().size();
        Member member = new OrdinaryMember("ordi", "o");
        guild.addMember(guildMaster, member);
        int afterAdd = guild.getMemberList().size();
        assertEquals(beforeAdd+1, afterAdd);
    }

    @Test
    public void testMethodRemoveMember() {
        //TODO: Complete me
        Member member = new OrdinaryMember("ordi", "o");
        guild.addMember(guildMaster, member);
        int beforeDelete = guild.getMemberList().size();
        guild.removeMember(guildMaster, member);
        int afterDelete = guild.getMemberList().size();
        assertEquals(beforeDelete - 1, afterDelete);
    }

    @Test
    public void testMethodMemberHierarchy() {
        Member sasuke = new OrdinaryMember("Asep", "Servant");
        guild.addMember(guildMaster, sasuke);
        assertEquals(guildMaster, guild.getMemberHierarchy().get(0));
        assertEquals(sasuke, guild.getMemberHierarchy().get(0).getChildMembers().get(0));
    }

    @Test
    public void testMethodGetMember() {
        //TODO: Complete me
        Member memberOrdinary = new OrdinaryMember("ordi", "o");
        Member memberPremium = new PremiumMember("pre", "p");
        guild.addMember(guildMaster, memberOrdinary);
        guild.addMember(guildMaster, memberPremium);
        Member memberOrdinaryCheck = guild.getMember(memberOrdinary.getName(), memberOrdinary.getRole());
        Member memberPremiumCheck = guild.getMember(memberPremium.getName(), memberPremium.getRole());
        assertEquals(memberOrdinary, memberOrdinaryCheck);
        assertEquals(memberPremium, memberPremiumCheck);
    }
}
