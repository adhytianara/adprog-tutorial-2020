package id.ac.ui.cs.advprog.tutorial5.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SoulTest {
    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul();
    }

    @Test
    public void testGetId() {
        long id = soul.getId();
        assertEquals(0, id);
    }

    @Test
    public void testGetName() {
        String name = "Soul";
        soul.setName(name);
        assertEquals(name, soul.getName());
    }

    @Test
    public void testGetAge() {
        int age = 20;
        soul.setAge(age);
        assertEquals(age, soul.getAge());
    }

    @Test
    public void testGetGender() {
        char gender = 'M';
        soul.setGender(gender);
        assertEquals(gender, soul.getGender());
    }

    @Test
    public void testGetOccupation() {
        String occupation = "King";
        soul.setOccupation(occupation);
        assertEquals(occupation, soul.getOccupation());
    }
}
