package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.LordranArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor.Armor;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LordranArmoryTest {

    Armory lordranArmory;

    @BeforeEach
    public void setUp(){
        lordranArmory = new LordranArmory();
    }

    @Test
    public void testcraftArmor(){
        // TODO create test
        assertTrue(lordranArmory.craftArmor() instanceof Armor);
    }

    @Test
    public void testcraftWeapon(){
        // TODO create test
        assertTrue(lordranArmory.craftWeapon() instanceof Weapon);
    }

    @Test
    public void learnSkill(){
        // TODO create test
        assertTrue(lordranArmory.learnSkill() instanceof Skill);
    }
}
