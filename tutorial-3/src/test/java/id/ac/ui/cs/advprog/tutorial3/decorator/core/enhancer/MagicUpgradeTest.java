package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Longbow;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class MagicUpgradeTest {


    private MagicUpgrade magicUpgrade;

    @BeforeEach
    public void setUp(){
        magicUpgrade = new MagicUpgrade(new Longbow());
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = magicUpgrade.getName();
        assertEquals(name, "Longbow");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String description = magicUpgrade.getDescription();
        assertEquals(description, "Big Longbow MagicUpgrade");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = magicUpgrade.getWeaponValue();
        assertTrue(30 <= value && value <= 35);
    }
}
