package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class OrdinaryMember implements Member {
    //TODO: Complete me

    String name, role;
    private List<Member> childList;

    public OrdinaryMember(String name, String role) {
        this.name = name;
        this.role = role;
        this.childList = new ArrayList<>();
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        return;
    }

    @Override
    public void removeChildMember(Member member) {
        return;
    }

    @Override
    public List<Member> getChildMembers() {
        return this.childList;
    }

    @Override
    public void addChildGuildMaster(Member child) {
        return;
    }

}
