package id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
public class GunTest {

    private Weapon weapon;

    @BeforeEach
    public void setUp(){
        weapon = new Gun();
    }

    @Test
    public void testMethodGetWeaponName(){
        //TODO: Complete me
        String name = weapon.getName();
        assertEquals(name, "Gun");
    }

    @Test
    public void testMethodGetWeaponDescription(){
        //TODO: Complete me
        String description = weapon.getDescription();
        assertEquals(description, "Automatic Gun");
    }

    @Test
    public void testMethodGetWeaponValue(){
        //TODO: Complete me
        int value = weapon.getWeaponValue();
        assertEquals(value, 20);
    }
}
