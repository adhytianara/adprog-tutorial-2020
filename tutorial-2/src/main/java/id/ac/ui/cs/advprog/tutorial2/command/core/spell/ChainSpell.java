package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    // TODO: Complete Me
    ArrayList<Spell> arSpell;

 
    public ChainSpell(ArrayList<Spell> arSpell){
        this.arSpell = arSpell;
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }

    @Override
    public void cast() {
        // TODO Auto-generated method stub
        for (Spell spell : arSpell) {
            spell.cast();
        }
    }

    @Override
    public void undo() {
        // TODO Auto-generated method stub
        for (int i = arSpell.size()-1; i >= 0; i--) {
            arSpell.get(i).undo();
        }
    }
}
