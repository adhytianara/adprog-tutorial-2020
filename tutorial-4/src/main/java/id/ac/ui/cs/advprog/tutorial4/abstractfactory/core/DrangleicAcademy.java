package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.Armory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.DrangleicArmory;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MajesticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.MetalClusterKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.SyntheticKnight;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.upgrade.Knight;

public class DrangleicAcademy extends KnightAcademy {
    private String name;

    public DrangleicAcademy(){
        this.name = "Drangleic";
    }

    @Override
    public String getName() {
        // TODO complete me
        return this.name;
    }

    @Override
    public Knight produceKnight(String type) {
        Knight knight = null;
        Armory armory = new DrangleicArmory();

        switch (type) {
            case "majestic":
                // TODO complete me
                knight = new MajesticKnight(armory);
                break;
            case "metal cluster":
                // TODO complete me
                knight = new MetalClusterKnight(armory);
                break;
            case "synthetic":
                // TODO complete me
                knight = new SyntheticKnight(armory);
                break;
        }

        return knight;
    }
}
