package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    // TODO: Complete Me

    public HighSpiritStealthSpell(HighSpirit hSpirit){
        super(hSpirit);
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Stealth";
    }

    @Override
    public void cast() {
        // TODO Auto-generated method stub
        spirit.stealthStance();
    }
}
